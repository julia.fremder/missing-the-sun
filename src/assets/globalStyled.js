import { createGlobalStyle } from 'styled-components';

const GlobalStyle = createGlobalStyle`


    *{
        padding:0;
        margin: 0;
        font-family: Arial, Helvetica, sans-serif;

    }

    #root{

        display: flex;
        flex-direction: column;
        min-height:100vh;
        overflow-y: scroll

        main {
            flex: 1;
        }
        

    }



`;

export default GlobalStyle;
