import http from '../config/http';

export const postCityService = (city) => http.post(`/weather/${city}`);