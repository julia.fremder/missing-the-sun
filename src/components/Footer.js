import React from 'react'
import { Container } from 'reactstrap'
import styled from 'styled-components'

const FooterComponent = () => {
  return (
    <Footer>
      <Container>Missing The Sun 2021 || Technical Assessment: Júlia Fremder</Container>
    </Footer>
  )
}

export default FooterComponent

const Footer = styled.footer`
display: flex;
flex-direction: column;
justify-content: center;
height: 6rem;
background-color: #eee;
`