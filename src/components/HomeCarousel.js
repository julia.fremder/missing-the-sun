import React from 'react';
import { Container, UncontrolledCarousel } from 'reactstrap';
import ReallyHot from '../assets/images/reallyHot.jpg';
import Chilly from '../assets/images/chilly.jpg';
import Hot from '../assets/images/hot.jpg';
import ReallyCold from '../assets/images/reallyCold.jpg';
import Warm from '../assets/images/warm.jpg';
import Cold from '../assets/images/cold.jpg';

const items = [
  {
    src: ReallyHot,
    altText: 'Desert feeling',
    caption: 'Desert feeling, drink some wather!',
    header: 'Really Hot',
    key: '1'
  },
  {
    src: Hot,
    altText: 'Sunny and hot',
    caption: 'Sunny and hot, remeber your sunscreen',
    header: 'Hot',
    key: '2'
  },
  {
    src: Warm,
    altText: 'Warm',
    caption: 'Warm, nice to take a walk outside!',
    header: 'Warm',
    key: '3'
  },
  {
    src: Chilly,
    altText: 'chilly and cloudy',
    caption: "Chilly, don't forget your coat and umbrella",
    header: 'Chilly',
    key: '4'
  },
  {
    src: Cold,
    altText: 'cold and cloudy',
    caption: "Cold, remeber your scarf!",
    header: 'Chilly',
    key: '5'
  },
  {
    src: ReallyCold,
    altText: 'Snowing',
    caption: 'Snow, try to stay safe!',
    header: 'Really Cold',
    key: '6'
  }
];

const HomeCarousel = () => <Container><UncontrolledCarousel style={{width: '50vw'}} items={items} /></Container>;

export default HomeCarousel;