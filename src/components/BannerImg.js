import React from 'react';
import SearchInput from './SearchInput';
import styled from 'styled-components';
import { Button, Container } from 'reactstrap';

const BannerImg = (props) => {
  return (
    <Banner>
      <Container><SearchInput {...props} />
      <Button className="btn-clean" size="sm" onClick={props.cleanCity}>Clean city</Button></Container>
    </Banner>
  );
};

export default BannerImg;

const Banner = styled.section`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  height: 10rem;
  padding: 2rem;
  background-color: #fafafa;
  .container{
    display: flex;
    .btn-clean {
      background-color: #47a8fe;
      width: 150px;
      border: none;
      outline: none;
      color: black;
      box-shadow: 1px 1px 2px grey;
      :focus {
        outline: none;
        box-shadow: 1px 1px 2px grey;
      }
    }
  }
`;
