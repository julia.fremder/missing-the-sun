import React from 'react'
import { Container } from 'reactstrap'
import styled from 'styled-components'

const HeaderComponent = () => {
  return (
    <Header>
      <Container><div>Missing The Sun</div><div>Check your city's temperature!</div></Container>
    </Header>
  )
}

export default HeaderComponent

const Header = styled.header`
display: flex;
flex-direction: column;
justify-content: center;
height: 5rem;
background-color: #47a8fe;

.container{
  display: flex;
  align-items: center;
  justify-content: space-between;
  height: 5rem;
}
`