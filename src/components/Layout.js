import React from 'react';
import HeaderComponent from './Header';
import FooterComponent from './Footer';

const Layout = ({ children }) => {
  return (
    <>
      <HeaderComponent />
      <main>{children}</main>
      <FooterComponent />
    </>
  );
};

export default Layout;
