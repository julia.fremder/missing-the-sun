import React from 'react';
import ReallyHot from '../assets/images/reallyHot.jpg';
import Chilly from '../assets/images/chilly.jpg';
import Hot from '../assets/images/hot.jpg';
import ReallyCold from '../assets/images/reallyCold.jpg';
import Warm from '../assets/images/warm.jpg';
import Cold from '../assets/images/cold.jpg';
import styled from 'styled-components';
import { Button, Container } from 'reactstrap';
import HomeCarousel from './HomeCarousel';

const WeatherInformation = ({ data }) => {
  const [path, setPath] = React.useState('');
  const [showCelsius, setShowCelsius] = React.useState(true);
  const srcImg = React.useCallback((data) => {
    if (data.temp_c && data.temp_c >= 40) {
      return setPath(ReallyHot);
    }
    if (30 <= data.temp_c && data.temp_c < 40) {
      return setPath(Hot);
    }
    if (20 <= data.temp_c && data.temp_c < 30) {
      return setPath(Warm);
    }
    if (10 <= data.temp_c && data.temp_c < 20) {
      return setPath(Chilly);
    }
    if (0 <= data.temp_c && data.temp_c < 10) {
      return setPath(Cold);
    }
    if (data.temp_c && data.temp_c < 0) {
      return setPath(ReallyCold);
    }
  }, []);

  React.useEffect(() => {
    srcImg(data);
  }, [srcImg, data]);

  const toggle = () => setShowCelsius(!showCelsius);

  return (
    <Weather>
      {data.name ? (
        <Container>
          <h2>{data.name}</h2>
          <div className="temperature">
            <p>
              Temperature now:{' '}
              {showCelsius ? `${data.temp_c}ºC` : `${data.temp_f}ºF`}
            </p>
            <Button size="sm" onClick={toggle}>
              {showCelsius ? 'Farenheit' : 'Celsius'}
            </Button>
          </div>

          <img src={path} alt="weather" />
        </Container>
      ) : (
        <HomeCarousel className="carousel" />
      )}
    </Weather>
  );
};

export default WeatherInformation;

const Weather = styled.section`
  .container {
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    padding: 2rem;
    img {
      width: 50vw;
      border: 4px solid #696969;
      border-radius: 4px;
      -webkit-box-shadow: 2px 2px 4px 1px rgba(0, 0, 0, 0.31);
      box-shadow: 2px 2px 4px 1px rgba(0, 0, 0, 0.31);
    }
    .btn {
      background-color: #47a8fe;
      border: none;
      outline: none;
      color: black;
      box-shadow: 1px 1px 2px grey;
      :focus {
        outline: none;
        box-shadow: 1px 1px 2px grey;
      }
    }
    .temperature {
      width: 100%;
      display: flex;
      align-items: center;
      justify-content: space-around;
      line-height: 1rem;
      margin-bottom: 1rem;
      p {
        font-size: 1rem;
        line-height: 1rem;
        margin: 0;
      }
    }
  }
  .w-100 {
    flex: 1;
    height:70vh;
  }
  
`;
