import { Button, Container } from 'reactstrap';
import React from 'react';
import { IoIosSearch } from 'react-icons/io';

import styled from 'styled-components';

const SearchInput = ({ handleChange, form, submitForm }) => {
  return (
    <SContainer>
      <div>
        <input
          id="input-city"
          onChange={handleChange}
          name="search"
          type="text"
          placeholder="city name"
          value={form?.search?.toLowerCase() || ''}
        />
        <Button type="submit" onClick={() => submitForm()}>
          <IoIosSearch />
        </Button>
      </div>
    </SContainer>
  );
};

export default SearchInput;

const SContainer = styled(Container)`
div{
  display: flex;
  flex-direction: row;
  align-items: center;
  background-color: #fafafa;
  border-radius: 2rem;
  padding: 0 1rem;
  width: 17rem;
  height: 2rem;
  box-shadow: 1px 1px 2px grey;
  svg {
    font-size: 1rem;
    margin-left: 1rem;
    color: grey;
  }
  .btn,
  .btn-secondary,
  [type='submit'] {
    background-color: transparent;
    color: black;
    border-radius: 5rem;
    border: none;
    outline: none !important;
    box-shadow: none;

    :focus {
      box-shadow: none;
    }
  }
  #input-city {
    background-color: #fafafa;
    border: none;
    height: 1.5rem;
    :focus {
      outline: none;
    }
  }
}
  
`;
