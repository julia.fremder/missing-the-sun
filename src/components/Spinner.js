import Spinner from 'react-bootstrap/Spinner'
import 'bootstrap/dist/css/bootstrap.min.css'
import styled from 'styled-components'

const Spinners = () => {
  return (
    <Loading>
      <Spinner animation="border" role="status" variant="dark" />
    </Loading>
  )
}
export default Spinners

export const Loading = styled.div`
width: 100%;
height: 100%;
display: flex;
align-items: center;
justify-content: center;
`