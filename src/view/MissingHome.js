import React from 'react';
import BannerImg from '../components/BannerImg';
import Layout from '../components/Layout';
import Spinner from '../components/Spinner'
import WeatherInformation from '../components/WeatherInformation';
import { postCityService } from '../services/city.service'

const MissingHome = () => {
  const [form, setForm] = React.useState({});
  const [loading, setLoading] = React.useState(false);
  const [cityWeather, setCityWeather] = React.useState({})
  const handleChange = (props) => {
    const { value, name } = props.target;
    setForm({
      ...form,
      [name]: value,
    });
  };
  const SpinnerLoading = () => loading ? <Spinner /> : ''
  
  const submitForm = React.useCallback(async () => {
    const city = form?.search?.toLowerCase()
    try {
      setLoading(true)
      const res = await postCityService(city)
      if(res.data.data.message) {
        setCityWeather(res.data.data.data)
        setLoading(false)
        setTimeout(() => {
          setForm({})
        }, 1000)
      } else {
        setCityWeather(res.data.data)
        setLoading(false)
        setTimeout(() => {
          setForm({})
        }, 1000)
      }
      console.log(res)
    } catch (error) {
      console.log(error, 'error')
      setLoading(false)
    }        
}, [form])

  const cleanCity = () => setCityWeather({})

  return (
    <Layout>
      <BannerImg cleanCity={cleanCity} form={form} handleChange={handleChange} submitForm={submitForm}/>
      {SpinnerLoading()}
      <WeatherInformation data={cityWeather} />
    </Layout>
  );
};

export default MissingHome;
