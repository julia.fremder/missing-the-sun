import React from 'react';
import ReactDOM from 'react-dom';
import GlobalStyle from './assets/globalStyled';
import 'bootstrap/dist/css/bootstrap.min.css';
import App from './App';


ReactDOM.render(
  <React.Fragment>
    <GlobalStyle />
    <App />
  </React.Fragment>,
  document.getElementById('root')
);

